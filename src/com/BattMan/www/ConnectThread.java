package com.BattMan.www;
import java.io.IOException;
import java.util.UUID;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.os.Handler;

public class ConnectThread extends Thread
{
	// Constants that indicate the current connection state
    public static final int STATE_NONE = 0;       // we're doing nothing
    public static final int STATE_LISTEN = 1;     // now listening for incoming connections
    public static final int STATE_CONNECTING = 2; // now initiating an outgoing connection
    public static final int STATE_CONNECTED = 3;  // now connected to a remote device
    
    boolean isRunning;
    
	private static final UUID MY_UUID_SECURE =UUID.fromString("00001101-0000-1000-8000-00805F9B34FB");
    private final BluetoothSocket mmSocket;
    private final BluetoothDevice mmDevice;
    private final BluetoothAdapter mBluetoothAdapter;
    private final Handler mHandler;
    private  ReaderThread readingBytsThread;
 
    public ConnectThread(BluetoothDevice device, BluetoothAdapter BluetoothAdapter, Handler handler )
    {
        // Use a temporary object that is later assigned to mmSocket,
        // because mmSocket is final
        BluetoothSocket tmp = null;
        mmDevice = device;
        mBluetoothAdapter = BluetoothAdapter;
        mHandler = handler;
 
        // Get a BluetoothSocket to connect with the given BluetoothDevice
        try
        {
            // MY_UUID is the app's UUID string, also used by the server code
            tmp = device.createRfcommSocketToServiceRecord(MY_UUID_SECURE);
        } 
        catch (IOException e) { }
        mmSocket = tmp;
    }
 
    public void run()
    {
        // Cancel discovery because it will slow down the connection
        mBluetoothAdapter.cancelDiscovery(); 
        try
        {
            // Connect the device through the socket. This will block
            // until it succeeds or throws an exception
            mmSocket.connect();
        } 
        catch (IOException connectException) {
            // Unable to connect; close the socket and get out
            try
            {
                mmSocket.close();
            }
            catch (IOException closeException) { }
            return;
        }
 
        // Do work to manage the connection (in a separate thread)
        manageConnectedSocket(mmSocket, mHandler);
    }
 
    private void manageConnectedSocket(BluetoothSocket mmSocket2, Handler handler)
    {
    	readingBytsThread = new ReaderThread(mmSocket2, handler);
    	readingBytsThread.start();
		
	}

	/** Will cancel an in-progress connection, and close the socket */
    public void cancel()
    {
            //mmSocket.close();
            readingBytsThread.cancel();
    }
}