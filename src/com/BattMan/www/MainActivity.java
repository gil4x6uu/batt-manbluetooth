package com.BattMan.www;

import java.util.Set;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.app.Activity;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import android.bluetooth.*;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;

public class MainActivity extends Activity
{
	public static final int MESSAGE_STATE_CHANGE = 1;
    public static final int MESSAGE_READ = 2;
    public static final int MESSAGE_WRITE = 3;
    public static final int MESSAGE_DEVICE_NAME = 4;
    public static final int MESSAGE_TOAST = 5;
    
    private final static int REQUEST_ENABLE_BT = 1;
    
    private static final String TAG = "BluetoothChat";
    private static final boolean D = true;
	
    private ConnectThread connectThread = null;
    private ListView devices;
    private ArrayAdapter <String> BArrayTadapter;
    private Button button;
    private Button connectButton;
    private TextView BTState;
    private BluetoothAdapter bluetooth;
    private BluetoothDevice battManDevice = null;
	
	
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        button = (Button)this.findViewById(R.id.button1);
        connectButton = (Button)this.findViewById(R.id.connectButton);
    	button.setOnClickListener(scanForDevices);
    	connectButton.setOnClickListener(connectToBattMan);
        BTState = ((TextView)MainActivity.this.findViewById(R.id.BTState));
    	bluetooth = BluetoothAdapter.getDefaultAdapter();
    	devices = (ListView) findViewById(R.id.devices);
    	BArrayTadapter=new ArrayAdapter <String>(MainActivity.this,android.R.layout.simple_list_item_1);
    	devices.setAdapter(BArrayTadapter);
    	findBluetoothState();
    	IntentFilter filter = new IntentFilter();
    	//filter.addAction(BluetoothDevice.ACTION_UUID);
    	filter.addAction(BluetoothDevice.ACTION_FOUND);
    	filter.addAction(BluetoothAdapter.ACTION_DISCOVERY_STARTED);
    	filter.addAction(BluetoothAdapter.ACTION_DISCOVERY_FINISHED);
    	registerReceiver(BR, filter);      
    }

    private void findBluetoothState()
    {
        
        if(bluetooth==null)
        {
    		BTState.setText("Bluetooth service not available in the device");
        }
        else
        {
    			if(bluetooth.isEnabled())
    			{
                    if(bluetooth.isDiscovering())
                    {
                            BTState.setText("Finding Devices");
                    }
                    else
                    {
                            BTState.setText("Bluetooth is Enabled");
                            BTState.setEnabled(true);
                    }
            	}
    			
                else
                {
                    BTState.setText("Bluetooth is not enabled");
                }
        }
       
    }

	 private final BroadcastReceiver BR=new BroadcastReceiver()
	 {
	        @Override
	        public void onReceive(Context context, Intent intent)
	        {
	                        String act=intent.getAction();
	                        
	                        if (BluetoothAdapter.ACTION_DISCOVERY_STARTED.equals(act))
	                        {
	                            //discovery starts, we can show progress dialog or perform other tasks
	                        	 BTState.setText("DISCOVERY_STARTED");
	                        } 
	                        
	                        else if (BluetoothAdapter.ACTION_DISCOVERY_FINISHED.equals(act))
	                        {
	                            //discovery finishes, dismis progress dialog
	                        	BTState.setText("DISCOVERY_FINISHED");
	                        }
	                        else if(BluetoothDevice.ACTION_FOUND.equals(act))
	                        {
	                                        BluetoothDevice device=intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
	                                        if(device.getName().equals("RN42-0322"))
	                                        {
	                                        	battManDevice = device;
	                                        }
	                                        BArrayTadapter.add(device.getName()+" - "+device.getAddress());
	                                        BArrayTadapter.notifyDataSetChanged();                               
	                        }
	        }
	};
	
	private final Handler mHandler = new Handler()
	{
        @Override
        public void handleMessage(Message msg)
        {
            switch (msg.what)
            {
            case MESSAGE_READ:
                byte[] readBuf = (byte[]) msg.obj;
                // construct a string from the valid bytes in the buffer
                String readMessage = new String(readBuf, 0, msg.arg1);
                BArrayTadapter.add(readMessage);
                break;
            }
        }
    };
	

	private Button.OnClickListener scanForDevices = new OnClickListener()
	{			
			@Override
	        public void onClick(View v)
	        {  		
                if(!bluetooth.isEnabled())
				{
                	Intent startBT=new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                    startActivityForResult(startBT,REQUEST_ENABLE_BT);
				}
                else if (bluetooth.isDiscovering())
                {
         		   // the button is pressed when it discovers, so cancel the discovery
                	bluetooth.cancelDiscovery();
                	BTState.setText("DISCOVERY_cancel");
                	findBluetoothState();
         	   	}
                else
                {
                	BArrayTadapter.clear();
                	bluetooth.startDiscovery();
	        		findBluetoothState();
                }
	        }
	};
	
	private Button.OnClickListener connectToBattMan = new OnClickListener()
	{			
			@Override
	        public void onClick(View v)
	        {
				if(connectThread!=null)
				{
					connectThread.cancel();
					connectButton.setText("connect");
					connectThread = null;
				}
				else
				{
					connectThread = new ConnectThread(battManDevice, bluetooth, mHandler);
					connectThread.start();
					connectButton.setText("disconnect");
					
				}
	        }
	};
	
	@Override
	public void onDestroy()
	{
        unregisterReceiver(BR);
        super.onDestroy();
    }
	
	 @Override
	 public boolean onCreateOptionsMenu(Menu menu)
	    
	 {
	        // Inflate the menu; this adds items to the action bar if it is present.
	        getMenuInflater().inflate(R.menu.main, menu);
	        return true;
	 }
	   
	    
}
